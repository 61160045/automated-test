const checkLength = function (password) {
  console.log(password)
  return password.length >= 8 && password.length <= 25
}

const checkAlphabet = function (password) {
  // const alphabets = 'abcdefghijklmnopqrstuvwxyz'
  // for (const ch of password) {
  //   if (alphabets.includes(ch.toLowercaseCase())) return true
  // }
  // return false
  return /[a-zA-Z]/.test(password)
}
const checkDigit = function (password) {
  const digits = '1234567890'
  for (const ch of password) {
    if (digits.includes(ch)) return true
  }
  return false
}
const checkSymbol = function (password) {
  const symbols = '!#$%&()*+,-./:;<=>?@[]^_`{|}~"'
  for (const ch of password) {
    if (symbols.includes(ch)) return true
  }
  return false
}
const checkPasssword = function (password) {
  return checkAlphabet(password) && checkLength(password) && checkDigit(password) && checkSymbol(password)
}

module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPasssword
}
