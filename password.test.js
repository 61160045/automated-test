const { checkLength, checkAlphabet, checkSymbol, checkPasssword, checkDigit } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 character to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 character to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 character to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digit in password', () => {
    expect(checkDigit('11111')).toBe(true)
  })

  test('should has not digit in password', () => {
    expect(checkDigit('maymae')).toBe(false)
  })
  test('should has digit 007 in password', () => {
    expect(checkDigit('maymae007')).toBe(true)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
})

describe('Test Password', () => {
  test('should password Kruram007++ to be true', () => {
    expect(checkPasssword('Kruram007++')).toBe(true)
  })
  test('should password maymae to be false', () => {
    expect(checkPasssword('maymae')).toBe(false)
  })
  test('should password maymae007 to be false', () => {
    expect(checkPasssword('maymae007')).toBe(false)
  })
  test('should password maymae@ to be false', () => {
    expect(checkPasssword('maymae@')).toBe(false)
  })
  test('should password maymae111111111111111111111111111111 to be false', () => {
    expect(checkPasssword('maymae111111111111111111111111111111')).toBe(false)
  })
})
